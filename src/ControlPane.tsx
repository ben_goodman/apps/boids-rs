import React from 'react'
import styled from 'styled-components'
import useLocalStorage from 'use-local-storage'
import { Tooltip } from 'react-tooltip'

const CollapsibleContainer = styled.div`
    display: flex;
    flex-direction: column;
    position: fixed;
    top: 0;
    left: 0;
    background-color: white;
    padding: 10px;
    background-color: unset;
`

const ControlPaneContainer = styled.div<{ $isOpen: boolean }>`
    display: ${({$isOpen}) => $isOpen ? 'block' : 'none'};
    background-color: white;
    padding: 10px;
    border: 1px solid black;
    background-color: unset;
`

export const ControlPane = () => {
    const [isPaneOpen, setPaneOpen] = useLocalStorage('isPaneOpen', true)

    const [boid_count, setBoidCount] = useLocalStorage<number|undefined>('boid_count', undefined)
    const [visualRange, setVisualRange] = useLocalStorage<number|undefined>('visual_range', undefined)
    const [protectedRange, setProtectedRange] = useLocalStorage<number|undefined>('protected_range', undefined)
    const [boundaryTurningForce, setBoundaryTurningForce] = useLocalStorage<number|undefined>('boundary_turning_force', undefined)
    const [centeringForce, setCenteringForce] = useLocalStorage<number|undefined>('centering_force', undefined)
    const [avoidanceForce, setAvoidanceForce] = useLocalStorage<number|undefined>('avoidance_force', undefined)
    const [alignmentForce, setAlignmentForce] = useLocalStorage<number|undefined>('alignment_force', undefined)
    const [maxSpeed, setMaxSpeed] = useLocalStorage<number|undefined>('max_speed', undefined)
    const [minSpeed, setMinSpeed] = useLocalStorage<number|undefined>('min_speed', undefined)
    const [fieldMargin, setFieldMargin] = useLocalStorage<number|undefined>('field_margin', undefined)
    const [drawFieldBox, setDrawFieldBox] = useLocalStorage<boolean|undefined>('draw_field_box', undefined)

    const clearLocalStorage = () => {
        localStorage.clear()
        window.location.reload()
    }

    return (
        <CollapsibleContainer>
            {/* minimize the control pane */}
            <button
                onClick={() => setPaneOpen(!isPaneOpen)}
            >
                {isPaneOpen ? 'Minimize' : 'Options'}
            </button>

            <ControlPaneContainer $isOpen={isPaneOpen}>
                {/* BOID COUNT */}
                <label
                    htmlFor='boid_count'
                    data-tooltip-id='boid_count'
                    data-tooltip-content='The number of boids to simulate.'
                >
                    Number of boids:
                </label>
                <Tooltip id='boid_count' />
                <input
                    name='boid_count'
                    type="number"
                    min="0"
                    max="1000"
                    value={boid_count}
                    onChange={(e) => setBoidCount(parseInt(e.target.value, 10))}
                />
                <br />

                {/* TURNING FORCE */}
                <label
                    htmlFor='turning'
                    data-tooltip='turning_force'
                    data-tooltip-content='Lower values produce sharper turns around the boundary.'
                >
                    Boundary Turning Force:
                </label>
                <Tooltip id='turning_force' />
                <input
                    name='turning'
                    type="number"
                    min="0"
                    max="1"
                    step="0.01"
                    value={boundaryTurningForce}
                    onChange={(e) => setBoundaryTurningForce(parseFloat(e.target.value))}
                />
                <br />
                <label htmlFor='visualRange'>Visual Range:</label>
                <input
                    name='visualRange'
                    type="number"
                    min="0"
                    max="1000"
                    value={visualRange}
                    onChange={(e) => setVisualRange(parseInt(e.target.value, 10))}
                />
                <br />
                <label htmlFor='protectedRange'>Protected Range:</label>
                <input
                    name='protectedRange'
                    type="number"
                    min="0"
                    max="1000"
                    value={protectedRange}
                    onChange={(e) => setProtectedRange(parseInt(e.target.value, 10))}
                />
                <br />
                <label htmlFor='centeringForce'>Centering Force:</label>
                <input
                    name='centeringForce'
                    type="number"
                    min="0"
                    max="1"
                    step="0.01"
                    value={centeringForce}
                    onChange={(e) => setCenteringForce(parseFloat(e.target.value))}
                />
                <br />
                <label htmlFor='avoidanceForce'>Avoidance Force:</label>
                <input
                    name='avoidanceForce'
                    type="number"
                    min="0"
                    max="1"
                    step="0.01"
                    value={avoidanceForce}
                    onChange={(e) => setAvoidanceForce(parseFloat(e.target.value))}
                />
                <br />
                <label htmlFor='alignmentForce'>Alignment Force:</label>
                <input
                    name='alignmentForce'
                    type="number"
                    min="0"
                    max="1"
                    step="0.01"
                    value={alignmentForce}
                    onChange={(e) => setAlignmentForce(parseFloat(e.target.value))}
                />
                <br />
                <label htmlFor='maxSpeed'>Max Speed:</label>
                <input
                    name='maxSpeed'
                    type="number"
                    min="0"
                    max="10"
                    step="0.1"
                    value={maxSpeed}
                    onChange={(e) => setMaxSpeed(parseFloat(e.target.value))}
                />
                <br />
                <label htmlFor='minSpeed'>Min Speed:</label>
                <input
                    name='minSpeed'
                    type="number"
                    min="0"
                    max="10"
                    step="0.1"
                    value={minSpeed}
                    onChange={(e) => setMinSpeed(parseFloat(e.target.value))}
                />
                <br />
                <label htmlFor='fieldMargin'>Field Margin:</label>
                <input
                    name='fieldMargin'
                    type="number"
                    min="0"
                    max="1000"
                    value={fieldMargin}
                    onChange={(e) => setFieldMargin(parseInt(e.target.value, 10))}
                />
                <br />
                <label htmlFor='drawFieldBox'>Draw Field Box:</label>
                <input
                    name='drawFieldBox'
                    type="checkbox"
                    checked={drawFieldBox}
                    onChange={(e) => setDrawFieldBox(e.target.checked)}
                />

                <hr/>

                <button
                    onClick={() => window.location.reload()}
                >Apply</button>

                <button onClick={clearLocalStorage}>Reset</button>

            </ControlPaneContainer>
        </CollapsibleContainer>
    )
}