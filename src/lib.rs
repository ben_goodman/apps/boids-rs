use std::cell::RefCell;
use std::f64;
use std::rc::Rc;
use wasm_bindgen::prelude::*;

mod boid;
mod random_sample;

use boid::Boid;
use random_sample::RandomSample;

fn draw_agent(agent: &Boid, context: &web_sys::CanvasRenderingContext2d) {
    context.begin_path();
    context
        .arc(agent.position.0, agent.position.1, 1.0, 0.0, f64::consts::PI * 2.0)
        .unwrap();
    context.stroke();
}

fn window() -> web_sys::Window {
    web_sys::window().expect("no global `window` exists")
}

fn request_animation_frame(f: &Closure<dyn FnMut()>) {
    window()
        .request_animation_frame(f.as_ref().unchecked_ref())
        .expect("should register `requestAnimationFrame` OK");
}

#[wasm_bindgen]
pub fn start(
    inner_width: f64,
    inner_height: f64,
    boid_count: u32,
    protected_range: f64,
    visual_range: f64,
    boundary_turning_force: f64,
    centering_force: f64,
    avoidance_force: f64,
    alignment_force: f64,
    min_speed: f64,
    max_speed: f64,
    field_margin: f64,
    draw_field_box: bool,
) {

    web_sys::console::log_1(&format!(
        "Starting boids.
        resolution: {}x{}
        boid_count: {}
        protected_range: {}
        visual_range: {}
        boundary_turning_force: {}
        centering_force: {}
        avoidance_force: {}
        alignment_force: {}
        min_speed: {}
        max_speed: {}
        field_margin: {}
        draw_field_box: {}
        ",
        inner_width,
        inner_height,
        boid_count,
        protected_range,
        visual_range,
        boundary_turning_force,
        centering_force,
        avoidance_force,
        alignment_force,
        min_speed,
        max_speed,
        field_margin,
        draw_field_box
    ).into());

    // create a canvas 2D context
    let document = web_sys::window().unwrap().document().unwrap();
    let canvas = document.get_element_by_id("canvas").unwrap();
    let canvas: web_sys::HtmlCanvasElement = canvas
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .map_err(|_| ())
        .unwrap();

    canvas.set_width(inner_width as u32);
    canvas.set_height(inner_height as u32);

    let field_box_vertices = vec![
        (field_margin, field_margin),
        (inner_width - field_margin, field_margin),
        (inner_width - field_margin, inner_height - field_margin),
        (field_margin, inner_height - field_margin),
    ];

    let context: web_sys::CanvasRenderingContext2d = canvas
        .get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<web_sys::CanvasRenderingContext2d>()
        .unwrap();

    // generate agents
    let mut agents = Vec::new();
    for id in 0..boid_count {
        let boid = Boid {
            id,
            position: (
                RandomSample::interval_inclusive(0.0, inner_width as f32).into(),
                RandomSample::interval_inclusive(0.0, inner_height as f32).into(),
            ),
            velocity: (0.0, 0.0),
            world_size: (inner_width, inner_height),
            field_margin,
            max_speed,
            min_speed,
            boundary_turning_force,
            avoidance_force,
            alignment_force,
            centering_force,
            protected_range,
            visual_range,
        };
        agents.push(boid);
    }

    // Create animation loop.
    // https://rustwasm.github.io/wasm-bindgen/examples/request-animation-frame.html
    let f = Rc::new(RefCell::new(None));
    let g = f.clone();

    *g.borrow_mut() = Some(Closure::wrap(Box::new(move || {

        context.clear_rect(0.0, 0.0, canvas.width() as f64, canvas.height() as f64);
        // draw the field box
        if draw_field_box {
            context.begin_path();
            context.move_to(field_box_vertices[0].0, field_box_vertices[0].1);
            for vertex in field_box_vertices.iter().skip(1) {
                context.line_to(vertex.0, vertex.1);
            }
            context.close_path();
            context.stroke();
        }

        // update agents
        let old_state = &agents.clone();
        for agent in agents.iter_mut() {
            agent.update(old_state);
            draw_agent(&agent, &context);
        }

        request_animation_frame(f.borrow().as_ref().unwrap());
    }) as Box<dyn FnMut()>));

    request_animation_frame(g.borrow().as_ref().unwrap());
}