pub struct RandomSample {}

impl RandomSample {
    pub fn interval_inclusive(min: f32, max: f32) -> f32 {
        min + (max - min) * js_sys::Math::random() as f32
    }
}
