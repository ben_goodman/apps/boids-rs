import React from 'react'
import { createRoot } from 'react-dom/client'
import { ControlPane } from './ControlPane'

// https://people.ece.cornell.edu/land/courses/ece4760/labs/s2021/Boids/Boids.html
const BOID_DEFAULTS = {
    boid_count: 300,
    boundary_turning_force: 0.25,
    visual_range: 100,
    protected_range: 10,
    centering_force: 0.0005,
    avoidance_force: 0.15,
    alignment_force : 0.05,
    max_speed: 7,
    min_speed: 1,
    field_margin: window.innerHeight / 4,
    draw_field_box: false
};

// initialize local storage with defaults
for (let [key, defaultValue] of Object.entries(BOID_DEFAULTS)) {
    const initValue: string|null = window.localStorage.getItem(key)
    if (initValue === null) {
        console.debug(`Initializing ${key} to ${defaultValue}.`)
        window.localStorage.setItem(key, defaultValue.toString())
    }
}


// import the bindings for the pre-built wasm module
import('../pkg').then(module => {
    const container = document.getElementById('root')
    const root = createRoot(container!);

    // initialize the module
    const start = () => module.start(
        window.innerWidth,
        window.innerHeight,
        parseInt(window.localStorage.getItem('boid_count')),
        parseInt(window.localStorage.getItem('protected_range')),
        parseInt(window.localStorage.getItem('visual_range')),
        parseFloat(window.localStorage.getItem('boundary_turning_force')),
        parseFloat(window.localStorage.getItem('centering_force')),
        parseFloat(window.localStorage.getItem('avoidance_force')),
        parseFloat(window.localStorage.getItem('alignment_force')),
        parseInt(window.localStorage.getItem('min_speed')),
        parseInt(window.localStorage.getItem('max_speed')),
        parseInt(window.localStorage.getItem('field_margin')),
        JSON.parse(window.localStorage.getItem('draw_field_box'))
    )

    // the control pane gets(sets) from(to) local storage
    root.render(<ControlPane/>)

    start()

})


