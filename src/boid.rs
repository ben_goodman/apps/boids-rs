#[derive(Clone, Copy)]
pub struct Boid {
    pub position: (f64, f64),
    pub id: u32,
    pub velocity: (f64, f64),
    pub world_size: (f64, f64),
    pub field_margin: f64,
    pub boundary_turning_force: f64,
    pub avoidance_force: f64,
    pub alignment_force: f64,
    pub centering_force: f64,
    pub protected_range: f64,
    pub visual_range: f64,
    pub max_speed: f64,
    pub min_speed: f64,
}

impl Boid {
    pub fn update(&mut self, agents: &Vec<Boid>) {
        self.apply_separation_force(&agents);
        self.apply_alignment_force(&agents);
        self.apply_cohesion_force(&agents);
        self.apply_boundary_force();
        self.clamp_velocity();
        self.position.0 += self.velocity.0;
        self.position.1 += self.velocity.1;
    }


    // 1. Separation Force
    // Each boid attempts to avoid running into other boids.
    // If two or more boids get too close to one another (i.e. within one another's protected range),
    // they will steer away from one another to avoid a collision.
    fn apply_separation_force (&mut self, agents: &Vec<Boid>) {
        let dv = agents.iter().fold(
            (0.0, 0.0),
            |mut acc, agent| {
                if self.id != agent.id {
                    let distance = ((self.position.0 - agent.position.0).powi(2) + (self.position.1 - agent.position.1).powi(2)).sqrt();
                    if distance < self.protected_range {
                        acc.0 += self.position.0 - agent.position.0;
                        acc.1 += self.position.1 - agent.position.1;
                    }
                }
                acc
            }
        );

        self.velocity.0 += dv.0 * self.avoidance_force;
        self.velocity.1 += dv.1 * self.avoidance_force;
    }

    // 2. Alignment Force
    // Each boid attempts to match the velocity of its neighbors.
    // If a boid sees that its neighbors are moving in a certain direction, it will attempt to move in that direction as well.
    fn apply_alignment_force (&mut self, agents: &Vec<Boid>) {
        let mut neighbors: i64 = 0;
        let mut dv = agents.iter().fold(
            (0.0, 0.0),
            |mut acc, agent| {
                if self.id != agent.id {
                    let distance = ((self.position.0 - agent.position.0).powi(2) + (self.position.1 - agent.position.1).powi(2)).sqrt();
                    if distance < self.visual_range {
                        neighbors += 1;
                        acc.0 += agent.velocity.0;
                        acc.1 += agent.velocity.1;
                    }
                }
                acc
            }
        );

        if neighbors > 0 {
            dv.0 /= neighbors as f64;
            dv.1 /= neighbors as f64;
        }

        self.velocity.0 += dv.0 * self.alignment_force;
        self.velocity.1 += dv.1 * self.alignment_force;
    }


    // 3. Cohesion Force
    // Each boid attempts to move towards the average position of its neighbors.
    // If a boid sees that its neighbors are clustered in a certain area, it will attempt to move towards that area.
    fn apply_cohesion_force (&mut self, agents: &Vec<Boid>) {
        let mut neighbors: i64 = 0;
        let mut group_position = agents.iter().fold(
            (0.0, 0.0),
            |mut acc, agent| {
                if self.id != agent.id {
                    let distance = ((self.position.0 - agent.position.0).powi(2) + (self.position.1 - agent.position.1).powi(2)).sqrt();
                    if distance < self.visual_range {
                        neighbors += 1;
                        acc.0 += agent.position.0;
                        acc.1 += agent.position.1;
                    }
                }
                acc
            }
        );

        if neighbors > 0 {
            group_position.0 /= neighbors as f64;
            group_position.1 /= neighbors as f64;
        }

        let dv = (
            group_position.0 - self.position.0,
            group_position.1 - self.position.1
        );

        self.velocity.0 += dv.0 * self.centering_force;
        self.velocity.1 += dv.1 * self.centering_force;
    }


    // 4. Clamp velocity - constrain boids to their min/max speeds
    fn clamp_velocity (&mut self) {
        let speed = (self.velocity.0.powi(2) + self.velocity.1.powi(2)).sqrt();
        if speed > self.max_speed {
            self.velocity.0 = (self.velocity.0 / speed) * self.max_speed;
            self.velocity.1 = (self.velocity.1 / speed) * self.max_speed;
        } else if speed < self.min_speed {
            self.velocity.0 = (self.velocity.0 / speed) * self.min_speed;
            self.velocity.1 = (self.velocity.1 / speed) * self.min_speed;
        }
    }


    // 4. Boundary Force
    // Each boid attempts to stay within a certain boundary.
    // If a boid sees that it is moving outside of a certain boundary, it will attempt to move back inside that boundary.
    fn apply_boundary_force (&mut self) {
        if self.position.0 < self.field_margin {
            self.velocity.0 += self.boundary_turning_force;
        }
        if self.position.0 > self.world_size.0 - self.field_margin {
            self.velocity.0 -= self.boundary_turning_force;
        }

        if self.position.1 < self.field_margin {
            self.velocity.1 += self.boundary_turning_force;
        }
        if self.position.1 > self.world_size.1 - self.field_margin {
            self.velocity.1 -= self.boundary_turning_force;
        }
    }


}